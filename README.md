# Project Mayhem IV

Project Mayhem IV is a revival of the Project Mayhem III skin for Kodi 19 and above.

## Huh?
When Kodi (as XBMC) launched for the Xbox about 20 years ago (happy birthday, Kodi!), the developers needed a skin that was fast, easy, and worked well with a controller as big as the Xbox's original pad. Community member Jezz_X stepped up to develop this new interface as Project Mayhem III. Eventually, years came and went, and Kodi was ported away from Xbox, onto more devices, for which new skins like [Confluence](https://kodi.tv/addons/matrix/skin.confluence/) and [Estuary](https://github.com/phil65/skin.estuary) were created. But I was bored and like the old skin, so I'm here doing this or something.

## Installation

Before installing PMIV, you'll need to enable "unknown sources" on your Kodi installation. If you don't feel comfortable doing this, don't worry; I'm planning to submit PMIV to the Kodi repository sometime in the near future.

Kodi has a wiki page on how to enable add-ons from unknown sources [here](https://kodi.wiki/view/Add-on_manager#How_to_install_from_a_ZIP_file).

## Usage

> Project Mayhem IV is in a pre-alpha development stage. This won't work perfectly!

Simply install and enjoy the vibes.

## Contributing
Merge requests are much appreciated! For major changes, please open an issue first to discuss what you would like to change. `^w^`

Translations are being ported from [Estuary](https://github.com/phil65/skin.estuary), but any updates you have are welcomed!

## License
This work is licensed under the Creative Commons Attribution-Share Alike 3.0 United States License.

To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/3.0/us/](http://creativecommons.org/licenses/by-sa/3.0/us/) or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

For more information, including any other possible licenses used by Project Mayhem IV, see [here](LICENSE.txt).